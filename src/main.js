import Vue from "vue";
import App from "./App.vue";
import router from "./router";

// Vee Validate
import { ValidationObserver, ValidationProvider, extend, localize } from 'vee-validate';
import { required, email, numeric } from 'vee-validate/dist/rules';
import en from 'vee-validate/dist/locale/en.json';

// Vue Sweet Alert 2
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faChevronLeft, faWindowClose, faBars, faAngleDown, faAngleRight, faIndustry, faBell, faFlask, faWater, faFileContract, faCircle,
  faCog, faSignOutAlt, faAngleDoubleLeft, faAngleLeft, faAngleDoubleRight, faPlus, faPencilAlt, faSave, faMinus, faExclamationTriangle,
  faCheckSquare } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// Vue Table 2
import Vuetable from 'vuetable-2'
import VuetablePagination from "./components/table/VuetablePagination";
import VuetablePaginationInfo from 'vuetable-2/src/components/VuetablePaginationInfo';

// Vue Switch
import PrettyCheck from 'pretty-checkbox-vue/check';

require('js-loading-overlay');

let VueCookie = require('vue-cookie');

// Add require font awesome icon globally.
library.add(faChevronLeft, faWindowClose, faBars, faAngleDown, faAngleRight, faIndustry, faBell, faFlask, faWater, faFileContract, faCircle,
    faCog, faSignOutAlt, faAngleDoubleRight, faAngleDoubleLeft, faAngleLeft, faPlus, faPencilAlt, faSave, faMinus, faExclamationTriangle,
    faCheckSquare);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('p-check', PrettyCheck);

// Vee validate rule and local.
localize('en', en);
extend('email', email);
extend('required', required);
extend('numeric', numeric);

// Load metis menu.
require('metismenu');

Vue.prototype.$JsLoadingOverlay = window.JsLoadingOverlay;
Vue.config.productionTip = false;

Vue.use(VueCookie);
Vue.use(VueSweetalert2);
Vue.use(PrettyCheck);

// Vee validate components.
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

// Vue table 2 components.
Vue.component('Vuetable', Vuetable);
Vue.component('VuetablePagination', VuetablePagination);
Vue.component('VuetablePaginationInfo', VuetablePaginationInfo);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
