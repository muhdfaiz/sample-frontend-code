import Vue from "vue";
import VueRouter from "vue-router";
import { resetSidebarMenu, highlightSidebarMenu } from '../utils/sidebar.js';

Vue.use(VueRouter);

const routes = [
  /**
   * All routes related to authentication.
   */
  {
    path: "/",
    name: "Auth",
    component: () =>
        import(/* webpackChunkName: "auth" */"@/views/authentication/Auth.vue")
  },
  /**
   * All routes related to productions.
   */
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () =>
        import(/* webpackChunkName: "dashboard" */"@/views/layouts/Master.vue"),
    children: [
      {
        path: 'preparations/dpnr',
        name: 'dpnr',
        component: () =>
            import(/* webpackChunkName: "preparations.dpnr" */"@/views/preparations/dpnr/Dpnr.vue")
      },
      {
        path: 'preparations/enr-25',
        name: 'enr-25',
        component: () =>
            import(/* webpackChunkName: "preparations.enr25" */"@/views/preparations/enr25/Enr25.vue")
      },
      {
        path: 'preparations/enr-50',
        name: 'enr-50',
        component: () =>
            import(/* webpackChunkName: "preparations.enr50" */"@/views/preparations/enr50/Enr50.vue")
      },
      {
        path: 'productions/dpnr',
        name: 'dpnr',
        component: () =>
            import(/* webpackChunkName: "productions.dpnr" */"@/views/productions/dpnr/Dpnr.vue")
      },
      {
        path: 'productions/enr-25',
        name: 'enr-25',
        component: () =>
            import(/* webpackChunkName: "productions.enr25" */"@/views/productions/enr25/Enr25.vue")
      },
      {
        path: 'productions/enr-50',
        name: 'enr-50',
        component: () =>
            import(/* webpackChunkName: "productions.enr50" */"@/views/productions/enr50/Enr50.vue")
      },
      {
        path: 'tanks',
        name: 'tanks',
        component: () =>
            import(/* webpackChunkName: "tanks" */"@/views/tanks/Tanks.vue")
      },
      {
        path: 'alert-logs',
        name: 'alert-logs',
        component: () =>
            import(/* webpackChunkName: "alert-logs" */"@/views/alert-logs/AlertLog.vue")
      },
    ]
  },
  {
    path: "*",
    name: "404",
    component: () =>
        import(/* webpackChunkName: "page-not-found" */"@/views/error/PageNotFound.vue")
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.afterEach(() => {
  resetSidebarMenu();
  // After detect route change, highlight the sidebar menu based on the page URL.
  highlightSidebarMenu();
});

export default router;
