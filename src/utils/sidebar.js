import $ from 'jquery';

/**
 * Initialize the sidebar menu using metismenu library
 */
export const initializeSidebarMenu = () => {
	$('#metismenu').metisMenu();

	if ($(window).width() < 992) {
		$('body').addClass("mini-sidebar");
	}

	// Minimalize menu when screen is less than 769px
	$(window).bind("resize", function () {
		let originalWidth = $(window).width();

		$(window).resize(function(){
			let newWidth = $(window).width();

			if(newWidth !== originalWidth){
				if (window.innerWidth  < 992) {
					$('body').addClass("mini-sidebar");
				} else {
					$('body').removeClass('mini-sidebar')
				}
				originalWidth = newWidth;
			}
		});

	});
};

/**
 * Highlight active sidebar menu. Need to trigger this function when route change.
 */
export const highlightSidebarMenu = () => {
	$(function () {
		$("#sidebar ul li a").each(function () {
			// Get current URL
			let urlPath = window.location.pathname;
			let menuLink = $(this).attr('href');

			if (urlPath === menuLink) {
				$(this).addClass("active");
				$(this).parent().addClass("active");
				$(this).parent().parent().addClass("mm-show");
				$(this).parent().parent().parent().addClass("mm-active");

				let markActive = '';

				if (typeof $(this).parent().find('.mark-active')[0] !== 'undefined') {
					markActive = $(this).parent().find('.mark-active')[0];
				} else {
					markActive = $(this).parent().parent().parent().find('.mark-active')[0];
				}

				if (!$(markActive).hasClass("active")) {
					$(markActive).show();
				}

			}
		});
	});
};

/**
 * Toggle sidebar from normal sidebar or mini sidebar or otherwise.
 */
export const toggleSidebar = () => {
	$(".sidebar-toggle").on("click", function () {
		$('body').toggleClass("mini-sidebar");
	})
};

/**
 * Reset active sidebar menu. Need to trigger this function when route change.
 */
export const resetSidebarMenu = () => {
	$('#sidebar').removeClass('active');
	$('#sidebar li').removeClass('active');

	// Reset active menu when route change.
	$('.mark-active').hide();
	$('li').removeClass('mm-active');
	$('ul').removeClass('mm-show');
	$("a.has-arrow").attr("aria-expanded","false");
};


export default {highlightSidebarMenu, resetSidebarMenu, toggleSidebar}
