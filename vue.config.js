const path = require("path");

module.exports = {
	css: {
		sourceMap: true,
		loaderOptions: {
			sass: {
				prependData: `@import "@/assets/styles/_variables.scss"; @import "@/assets/styles/mixin/_media-query.scss";`
			}
		}
	}
};
